package com.example.polymorphicrest

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PolymorphicSpec extends Specification {
    @LocalServerPort
    int port

    @Autowired
    TestRestTemplate restTemplate

    @Autowired
    ObjectMapper objectMapper

    def 'When a finished good component is posted, it is deserialized correctly'() {
        when:
        def result = restTemplate.postForEntity("http://localhost:${port}", [
            "name"         : "foo",
            "componentType": "FINISHED_GOOD",
            "productId"    : 12345
        ], String)

        then:
        result.body == "Received component type FinishedGoodComponent"
    }

    def 'When an invalid finished good component is posted, an HTTP 400 status is returned'() {
        when:
        def result = restTemplate.postForEntity("http://localhost:${port}", [
            name         : "foo",
            componentType: "FINISHED_GOOD",
            productId    : -12345
        ], String)

        then:
        result.statusCodeValue == 400
    }

    def 'When an invalid component type is provided, an HTTP 400 status is returned'() {
        when:
        def result = restTemplate.postForEntity("http://localhost:${port}", [
            name         : "foo",
            componentType: "FOO"
        ], String)

        then:
        result.statusCodeValue == 400
    }

    def 'When a sub-recipe component is posted, it is deserialized correctly'() {
        when:
        def result = restTemplate.postForEntity("http://localhost:${port}", [
            name         : "foo",
            componentType: "SUB_RECIPE",
            someId       : 12345
        ], String)

        then:
        result.body == "Received component type SubRecipeComponent"
    }

    def 'When an ingredient component is posted, it is deserialized correctly'() {
        when:
        def result = restTemplate.postForEntity("http://localhost:${port}", [
            name           : "foo",
            componentType  : "INGREDIENT",
            isSubstitutable: true
        ], String)

        then:
        result.body == "Received component type IngredientComponent"
    }
}
