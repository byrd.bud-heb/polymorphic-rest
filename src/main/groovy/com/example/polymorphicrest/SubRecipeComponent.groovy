package com.example.polymorphicrest

import javax.validation.constraints.Positive

class SubRecipeComponent implements Component {
    @Positive
    int someId
}
