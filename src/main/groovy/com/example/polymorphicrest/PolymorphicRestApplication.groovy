package com.example.polymorphicrest

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid

@RestController
@SpringBootApplication
class PolymorphicRestApplication {
    static void main(String[] args) {
        SpringApplication.run(PolymorphicRestApplication, args)
    }

    @PostMapping("/")
    String post(@Valid @RequestBody Component component) {
        return "Received component type ${component.getClass().getSimpleName()}"
    }
}
