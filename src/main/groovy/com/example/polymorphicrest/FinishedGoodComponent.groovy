package com.example.polymorphicrest

import javax.validation.constraints.Positive

class FinishedGoodComponent implements Component {
    @Positive
    int productId
}
