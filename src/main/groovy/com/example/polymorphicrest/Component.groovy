package com.example.polymorphicrest

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "componentType", visible = true)
@JsonSubTypes([
    @JsonSubTypes.Type(value = FinishedGoodComponent, name = "FINISHED_GOOD"),
    @JsonSubTypes.Type(value = SubRecipeComponent, name = "SUB_RECIPE"),
    @JsonSubTypes.Type(value = IngredientComponent, name = "INGREDIENT")
])
trait Component {
    @NotEmpty
    String name

    @NotNull
    ComponentType componentType
}
