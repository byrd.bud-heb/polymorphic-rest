package com.example.polymorphicrest

enum ComponentType {
    FINISHED_GOOD,
    SUB_RECIPE,
    INGREDIENT
}
